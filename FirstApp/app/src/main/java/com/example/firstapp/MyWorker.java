package com.example.firstapp;

/**
 * @author: lionel.mangoua
 * date: 01/07/2020
 */

public class MyWorker {

    public static int doubleTheValue(int value) {
        return value * 2;
    }
}
